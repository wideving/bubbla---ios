import BackgroundTasks
import Combine
import Foundation
import UserNotifications

class NewsUpdaterTask {
    static let identifier = "com.wideving.frihetligt.update_news"

    static let shared: NewsUpdaterTask = NewsUpdaterTask()

    private var cancellable: Cancellable?
    private let itemLoader = ItemLoader()

    private init() {}

    func registerBackgroundWork() {
        BGTaskScheduler.shared.register(
            forTaskWithIdentifier: NewsUpdaterTask.identifier,
            using: nil
        ) { task in
            self.updateNews(task: task as! BGAppRefreshTask)
        }
    }

    private func updateNews(task: BGAppRefreshTask) {
        task.expirationHandler = {
            self.cancellable?.cancel()
        }

        cancellable = itemLoader.load(.latest)
            .sink(receiveCompletion: { _ in
                task.setTaskCompleted(success: true)
            }, receiveValue: { items in
                FileStorage().write(items: items)
            })

        scheduleBackgroundWork()
    }

    func scheduleBackgroundWork() {
        let taskRequest = BGAppRefreshTaskRequest(identifier: NewsUpdaterTask.identifier)
        taskRequest.earliestBeginDate = Date(timeIntervalSinceNow: 60 * 60)

        do {
            try BGTaskScheduler.shared.submit(taskRequest)
        } catch {
            print(error.localizedDescription)
        }
    }
}
