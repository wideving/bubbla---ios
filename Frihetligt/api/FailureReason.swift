import Foundation

enum FailureReason: Error {
    case noResponse
    case decode
}
