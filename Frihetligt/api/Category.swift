import Foundation

enum Category: String, CaseIterable {
    case latest = "nyheter"
    case world = "varlden"
    case sweden = "sverige"
    case misc = "blandat"
    case media = "media"
    case politics = "politik"
    case opinions = "opinion"
    case europe = "europa"
    case northAmerica = "nordamerika"
    case latinAmerica = "latinmerika"
    case asia = "asien"
    case middleEast = "mellanostern"
    case africa = "afrika"
    case economy = "ekonomi"
    case tech = "teknik"
    case sciene = "vetenskap"

    var displayTitle: String {
        switch self {
        case .latest:
            return "Senaste nyheterna"
        case .world:
            return "Världen"
        case .sweden:
            return "Sverige"
        case .misc:
            return "Blandat"
        case .media:
            return "Media"
        case .politics:
            return "Politik"
        case .opinions:
            return "Opinion"
        case .europe:
            return "Europa"
        case .northAmerica:
            return "Nordamerika"
        case .latinAmerica:
            return "Latinamerika"
        case .asia:
            return "Asien"
        case .middleEast:
            return "Mellanöstern"
        case .africa:
            return "Afrika"
        case .economy:
            return "Ekonomi"
        case .tech:
            return "Teknik"
        case .sciene:
            return "Vetenskap"
        }
    }
}
