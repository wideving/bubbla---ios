import Combine
import Foundation
import XMLParsing

class ItemLoader {
    private let baseUrl = "https://bubb.la/rss/"
    private let urlSession = URLSession.shared

    func load(_ category: Category) -> AnyPublisher<[Item], FailureReason> {
        return urlSession
            .dataTaskPublisher(for: URL(string: baseUrl + category.rawValue)!)
            .tryMap { data, response -> Data in
                guard let httpResponse = response as? HTTPURLResponse,
                    httpResponse.statusCode == 200 else {
                    throw FailureReason.noResponse
                }
                return data
            }
            .mapError { _ in FailureReason.noResponse }
            .tryMap {
                try XMLDecoder().decode(Rss.self, from: $0).channel.item
            }
            .mapError { _ in FailureReason.decode }
            .eraseToAnyPublisher()
    }
}
