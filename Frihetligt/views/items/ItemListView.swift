import SwiftUI
import SwiftUIRefresh

struct ItemListView: View {
    @EnvironmentObject var viewModel: ItemsViewModel
    @State var isShowingActionSheet: Bool = false
    @State var selectedItem: Item?

    init() {
        UITableView.appearance().tableFooterView = UIView()
    }

    var body: some View {
        NavigationView {
            ZStack {
                List(self.viewModel.items, id: \.id) { item in
                    ItemView(item: item)
                        .padding()
                        .onTapGesture {
                            self.selectedItem = item
                            self.isShowingActionSheet = true
                        }
                }
                .navigationBarItems(trailing: CategoriesButton())
                .navigationBarTitle(Text("Frihetligt"), displayMode: .inline)

                if self.viewModel.items.isEmpty {
                    NoNewsView()
                }
            }
        }
        .background(PullToRefresh(action: {
            self.viewModel.loadNews()
        }, isShowing: self.$viewModel.isLoading))
        .onAppear {
            self.viewModel.loadNews()
        }
        .actionSheet(isPresented: self.$isShowingActionSheet) {
            ActionSheetHelper.sheetWith(item: self.selectedItem!)
        }
        .alert(isPresented: self.$viewModel.isShowingError) {
            Alert(
                title: Text("Nätverksfel"),
                message: Text("Kunde inte hämta nyheter"),
                primaryButton: .default(Text("Försök igen"), action: {
                    self.viewModel.loadNews()
        }),
                secondaryButton: .default(Text("Avbryt"), action: {
                    self.viewModel.isLoading = false
        })
            )
        }
    }
}

struct ItemListView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ItemListView()
                .environmentObject(ItemsViewModel())
                .environment(\.colorScheme, .dark)

            ItemListView()
                .environmentObject(ItemsViewModel())
                .environment(\.colorScheme, .light)
        }
    }
}
