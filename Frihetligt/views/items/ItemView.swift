import SwiftUI

struct ItemView: View {
    var item: Item

    private var date: String {
        let formatter = DateFormatter()
        let locale = Locale(identifier: "sv")
        formatter.locale = locale
        formatter.dateStyle = .full
        let date = formatter.string(from: item.publishDate)
        return date.capitalized(with: locale)
    }

    var body: some View {
        VStack {
            VStack(alignment: .leading, spacing: 10) {
                HStack(alignment: .lastTextBaseline, spacing: 10) {
                    Text(item.category)
                        .font(.footnote)
                        .foregroundColor(.red)
                    Text(date)
                        .foregroundColor(.gray)
                        .font(.footnote)
                }
                Text(item.title)
                    .fixedSize(horizontal: false, vertical: true)
            }
        }
        .contentShape(Rectangle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ItemView(item: Item.example)
    }
}
