import Combine
import Foundation

class ItemsViewModel: ObservableObject {
    @Published var items: [Item]
    @Published var category: Category = .latest
    @Published var isShowingError: Bool = false
    @Published var isLoading: Bool = false

    private var storage = FileStorage()

    private var cancellable: Cancellable?

    init() {
        items = storage.read()
    }

    func loadNews() {
        isLoading = true
        cancellable = $category
            .setFailureType(to: FailureReason.self)
            .flatMap { category in
                ItemLoader().load(category)
            }
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
                self.isLoading = false
                self.isShowingError = true
            }) { items in
                if self.category == .latest {
                    self.storage.write(items: items)
                }

                self.isLoading = false
                self.items = items
            }
    }
}
