import SwiftUI

struct CategoriesButton: View {
    @EnvironmentObject var viewModel: ItemsViewModel
    @State var isShowingCategorySheet: Bool = false

    var body: some View {
        Button(action: {
            self.isShowingCategorySheet = true
        }, label: {
            Image(systemName: "line.horizontal.3.decrease.circle")
                .foregroundColor(.red)
    })
            .sheet(isPresented: $isShowingCategorySheet) {
                CategoriesView()
                    .environmentObject(self.viewModel)
            }
    }
}

struct CategoriesButton_Previews: PreviewProvider {
    static var previews: some View {
        CategoriesButton()
            .environmentObject(ItemsViewModel())
    }
}
