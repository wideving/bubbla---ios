import SwiftUI

class ActionSheetHelper {
    private static func openUrl(_ url: URL) {
        UIApplication.shared.open(
            url,
            options: [:],
            completionHandler: nil
        )
    }

    static func sheetWith(item: Item) -> ActionSheet {
        var buttons = [ActionSheet.Button]()

        buttons.append(.default(Text("Safari")) {
            self.openUrl(item.link)
    })

        if let link = item.facebook {
            buttons.append(.default(Text("Facebook")) {
                self.openUrl(link)
      })
        }

        if let link = item.reddit {
            buttons.append(.default(Text("Reddit")) {
                self.openUrl(link)
      })
        }

        if let link = item.twitter {
            buttons.append(.default(Text("Twitter")) {
                self.openUrl(link)
      })
        }

        if let link = item.soundcloud {
            buttons.append(.default(Text("Soundcloud")) {
                self.openUrl(link)
      })
        }

        buttons.append(.cancel())

        return ActionSheet(
            title: Text("Öppna med"),
            message: nil,
            buttons: buttons
        )
    }
}
