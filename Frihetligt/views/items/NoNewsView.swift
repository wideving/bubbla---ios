import SwiftUI

struct NoNewsView: View {
    @EnvironmentObject var itemsViewModel: ItemsViewModel

    var body: some View {
        VStack {
            Image(systemName: "wifi.slash")
                .resizable()
                .frame(width: 100, height: 100, alignment: .center)
                .foregroundColor(.red)
                .padding()

            Text("Inga nyheter hämtade")
                .font(.body)
        }
    }
}

struct NoNewsView_Previews: PreviewProvider {
    static var previews: some View {
        NoNewsView()
            .environmentObject(ItemsViewModel())
            .environment(\.colorScheme, .light)
    }
}
