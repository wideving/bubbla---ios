import SwiftUI

struct CategoryView: View {
    var category: Category

    @State var checkMarkVisible: Bool = false

    var body: some View {
        HStack {
            Text(category.displayTitle)
            Spacer()
            if self.checkMarkVisible {
                Image(systemName: "checkmark")
                    .foregroundColor(.red)
            }
        }
        .contentShape(Rectangle())
    }
}

struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView(category: .latest, checkMarkVisible: true)
            .previewLayout(.fixed(width: 300, height: 70))
    }
}
