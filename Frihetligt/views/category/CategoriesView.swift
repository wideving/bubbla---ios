import Combine
import SwiftUI

struct CategoriesView: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var viewModel: ItemsViewModel

    var categories: [Category] = Category.allCases

    var body: some View {
        NavigationView {
            List(categories, id: \.self) { category in
                CategoryView(
                    category: category,
                    checkMarkVisible: self.viewModel.category == category
                )
                .padding()
                .onTapGesture {
                    self.viewModel.category = category
                    self.viewModel.loadNews()
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
            .navigationBarTitle(Text("Välj kategori"), displayMode: .inline)
            .navigationBarItems(leading:
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }, label: {
                    Text("Klar")
        })
                    .foregroundColor(.red))
        }
    }
}

struct CategoriesView_Previews: PreviewProvider {
    static var previews: some View {
        CategoriesView()
            .environmentObject(ItemsViewModel())
    }
}
