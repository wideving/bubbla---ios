import Combine
import Foundation

class FileStorage {
    private let encoder: PropertyListEncoder = {
        let plistEncoder = PropertyListEncoder()
        plistEncoder.outputFormat = .xml
        return plistEncoder
    }()

    private let decoder = PropertyListDecoder()

    private let path: URL = {
        FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask
        )[0].appendingPathComponent("Storage.plist")
    }()

    func read() -> [Item] {
        guard
            let data = try? Data(contentsOf: path),
            let items = try? decoder.decode([Item].self, from: data)
        else {
            return []
        }

        return items
    }

    func write(items: [Item]) {
        guard let data = try? encoder.encode(items) else { return }
        try? data.write(to: path)
    }
}
