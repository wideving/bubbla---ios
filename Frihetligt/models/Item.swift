import Foundation

struct Item: Identifiable {
    let id: String
    let facebook: URL?
    let twitter: URL?
    let reddit: URL?
    let soundcloud: URL?
    let title: String
    let link: URL
    let category: String
    let publishDate: Date

    static var example: Item {
        return Item(
            id: UUID().uuidString,
            facebook: URL(string: "http://www.google.com")!,
            twitter: URL(string: "http://www.google.com")!,
            reddit: URL(string: "http://www.google.com")!,
            soundcloud: URL(string: "http://www.google.com")!,
            title: "This is a title",
            link: URL(string: "http://www.google.com")!,
            category: "Europe",
            publishDate: Date()
        )
    }
}

extension Item: Codable {
    private enum CodingKeys: String, CodingKey {
        case facebook = "social:facebook"
        case twitter = "social:twitter"
        case reddit = "social:reddit"
        case soundcloud = "social:soundcloud"
        case title
        case link
        case category
        case publishDate = "pubDate"
        case id = "guid"
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if let facebook = facebook {
            try container.encode(facebook.absoluteString, forKey: .facebook)
        }
        if let twitter = twitter {
            try container.encode(twitter.absoluteString, forKey: .twitter)
        }

        if let reddit = reddit {
            try container.encode(reddit.absoluteString, forKey: .reddit)
        }

        if let soundcloud = soundcloud {
            try container.encode(soundcloud.absoluteString, forKey: .soundcloud)
        }

        try container.encode(title, forKey: .title)
        try container.encode(link.absoluteString, forKey: .link)
        try container.encode(category, forKey: .category)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss Z"
        try container.encode(dateFormatter.string(from: publishDate), forKey: .publishDate)
        try container.encode(id, forKey: .id)
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        link = URL(string: try container.decode(String.self, forKey: .link))!
        category = try container.decode(String.self, forKey: .category)

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, dd MMM yyyy HH:mm:ss Z"
        publishDate = dateFormatter.date(
            from: try container.decode(String.self, forKey: .publishDate)
        )!

        id = try container.decode(String.self, forKey: .id)

        if container.contains(.facebook) {
            facebook = URL(string: try container.decode(String.self, forKey: .facebook))
        } else {
            facebook = nil
        }

        if container.contains(.twitter) {
            twitter = URL(string: try container.decode(String.self, forKey: .twitter))
        } else {
            twitter = nil
        }

        if container.contains(.reddit) {
            reddit = URL(string: try container.decode(String.self, forKey: .reddit))
        } else {
            reddit = nil
        }

        if container.contains(.soundcloud) {
            soundcloud = URL(string: try container.decode(String.self, forKey: .soundcloud))
        } else {
            soundcloud = nil
        }
    }
}
